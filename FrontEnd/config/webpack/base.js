const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const variables = require('../variable');
const { paths, urls, hosts, ports } = variables;
const INDEX_NOT_FOUND = -1;
const webpackHotMiddleWare = 'webpack-hot-middleware/client?reload=true';

module.exports = {
  /* Settings that used to shared between config files */
  settings: {
    paths,
    urls,
    hosts,
    ports,
    webpackHotMiddleWare
  },

  /* Webpack config */
  webpack: {
    entry: {
      polyfill: path.join(paths.config, 'preboot', 'polyfill.js'),
      vendor: path.join(paths.config, 'preboot', 'vendor.js'),
      app: path.join(paths.source, 'index.ts')
    },
    output: {
      path: paths.dist,
      filename: 'js/[name].js',
      publicPath: '/'
    },
    module: {
      rules: [
        /* Typescript */
        {
          test: /\.ts$/,
          loaders: [
            'awesome-typescript-loader',
            'angular2-template-loader',
            'angular-router-loader'
          ],
          exclude: [/\.(spec|e2e)\.ts$/]
        },

        /* HTML */
        {
          test: /\.html$/,
          loader: 'html-loader'
        },

        /* images */
        {
          test: /\.(jpg|jpe?g|gif|png|ico)$/,
          include: paths.app,
          loader: `url-loader?limit=12288&name=${urls.image}/[name].[hash].[ext]`
        },

        /* fonts */
        {
          test: /\.(woff|woff2|eot|ttf|svg).*$/,
          include: paths.app,
          loader: `url-loader?name=${urls.font}/[name].[hash].[ext]`
        }
      ]
    },
    resolve: {
      extensions: ['.js', '.ts', '.html', '.css', '.scss'],
      modules: [paths.app, 'node_modules']
    },
    plugins: [
      /* Workaround for angular/angular#11580 */
      new webpack.ContextReplacementPlugin(
        // The (\\|\/) piece accounts for path separators in *nix and Windows
        /angular(\\|\/)core(\\|\/)@angular/,
        paths.source
      ),

      /* Used to split out our sepcified vendor script */
      new webpack.optimize.CommonsChunkPlugin({
        names: ['polyfill', 'vendor'],
        filename: 'js/[name].js',
        publicPath: '/',
        minChunks: ({resource}) => (
          // This assumes your vendor imports exist in the node_modules directory
          resource &&
          resource.indexOf('node_modules') !== INDEX_NOT_FOUND &&
          resource.match(/\.(js|ts)$/)
        )
      }),

      /* CommonChunksPlugin will now extract all the common modules from polyfill, vendor, app, webpack,... bundles
       * NOTICE: Remove following manifest might impact on hot-reload build time!
       */
      new webpack.optimize.CommonsChunkPlugin({
        /* But since there are no more common modules between them we end up with
         * just the runtime code included in the manifest file
         */
        name: 'manifest',
        minChunks: Infinity
      }),

      /* Auto inject bundled js files to index.html */
      new HtmlWebpackPlugin({
        template: path.join(paths.source, 'index.html')
      })
    ]
  }
};
