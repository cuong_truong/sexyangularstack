/* eslint-disable no-console*/
const { npm_lifecycle_event } = process.env;
const target = npm_lifecycle_event || '';

console.log("🌎 🌎 🌎  *** Running %s command *** 🌎 🌎 🌎", target.toUpperCase());

switch (target.toLowerCase()) {
  case 'dev':
    console.log("🌎 🌎 🌎  *** DEVELOPMENT mode starts up *** 🌎 🌎 🌎");
    const dev = require('./dev');

    process.env.NODE_ENV = 'development';
    module.exports = dev;
    break;

  case 'start':
  case 'build':
  case 'prod':
    console.log("🌎 🌎 🌎  *** PRODUCTION mode starts up *** 🌎 🌎 🌎");  
    const prod = require('./prod');

    process.env.NODE_ENV = 'production'
    module.exports = prod;
    break;
}
