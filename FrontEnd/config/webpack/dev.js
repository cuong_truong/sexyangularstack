const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const CircularDependencyPlugin = require('circular-dependency-plugin');

const baseConfig = require('./base');
const ENV = process.env.NODE_ENV = process.env.ENV = 'development';

module.exports = merge(baseConfig, {
  settings: {
    host: baseConfig.settings.hosts.app,
    port: baseConfig.settings.ports.app
  },
  webpack: {
    devtool: 'source-map',
    entry: {
      webpack: [baseConfig.settings.webpackHotMiddleWare]
    },
    module: {
      rules: [
        {
          test: /\.(css|scss)$/,
          exclude: /\.async\.(css|scss)$/,
          loaders: [
            'raw-loader',
            'autoprefixer-loader?{browsers:["last 2 versions", "ie 6-8", "Firefox > 20"]}',
            'sass-loader?outputStyle=expanded'
          ]
        }
      ]
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      /* Circular dependency checking */
      new CircularDependencyPlugin({
        // Exclude detection of files based on a RegExp
        exclude: /a\.(js|ts)|node_modules/,
        // Add errors to webpack instead of warnings
        failOnError: true
      }),
      new webpack.DefinePlugin({
        'process.env': {
          ENV: JSON.stringify(ENV)
        }
      })
    ]
  }
});
