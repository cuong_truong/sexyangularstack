const path = require('path');
const dirRoot = path.resolve(__dirname, '..', '..');

/**
* Convert path to url
* @param  {String} filePath       file path string
* @return {String}                file url
*/
const pathToUrl = (filePath) => {
  return (filePath || '').replace(/\\/g, '/');
};

/**
* Generate path start with root directory based on args
* @param  {Array} args    path parts
* @return {String}        path string
*/
const root = (...args) => {
  return path.join.apply(path, [dirRoot].concat(args));
};

/**
* Generate url based on args
* @param  {Array} args    url parts
* @return {String}        url string
*/
const url = (...args) => {
  return pathToUrl(args.join('/'));
};

/**
* Export helper methods
*/
module.exports = {
  url,
  pathToUrl,
  root
};