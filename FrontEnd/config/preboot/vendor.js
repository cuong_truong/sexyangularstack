/* Third party code you want to preboot herehere
 * like angular, rxjs, bootstrap, lodash...
 * NOTICE: Once hot-reload is slow when code changed, it might be relating to third parties
 * being recaculated. Should consider to add third party here!
 */

// // Angular
// Import '@angular/common';
// Import '@angular/core';
// Import '@angular/forms';
// Import '@angular/http';
// Import '@angular/platform-browser';
// Import '@angular/platform-browser-dynamic';
// Import '@angular/router';

// // RxJS
// Import 'rxjs';

// Material
import '@angular/material';
