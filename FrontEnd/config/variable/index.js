const path = require('path');
const helper = require('../helper');

/* Paths info */
const paths = {
  /* FrontEnd/ */
  root: helper.root(),
  /* FrontEnd/_dist */
  dist: helper.root('_dist'),
  /* FrontEnd/config */
  config: helper.root('config'),
  /* FrontEnd/server */
  sever: helper.root('server'),
  /* FrontEnd/src */
  source: helper.root('src'),
  /* FrontEnd/typings */
  typings: helper.root('typings'),
  /* FrontEnd/src/app */
  app: helper.root('src', 'app'),
  /* FrontEnd/src/app/asset */
  asset: helper.root('src', 'app', 'asset')
};

/* Urls info */
const urls = {
  image: helper.url('asset', 'image'),
  font: helper.url('asset', 'font'),
  style: helper.url('asset', 'style')
};

/* Hosts info */
const hosts = {
  app: process.env.HOST || '0.0.0.0'
};

/* Ports info */
const ports = {
  app: process.env.PORT || 3000,
  prod: process.env.PORT_PROD || 3001,
  test: process.env.PORT_TEST || 9000
};

/* Export info */
module.exports = {
  paths,
  urls,
  hosts,
  ports
};
