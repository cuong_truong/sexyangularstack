const path = require('path');
const express = require('express');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const config = require('../config/webpack');
const app = express();
const host = config.settings.host;
const port = config.settings.port;
const env = process.env.NODE_ENV || 'development';
const isDevMode = env.toLowerCase() === 'development';
let compiler;

// Case DEV mode
if (isDevMode) {
  const webpackConfig = config.webpack;

  compiler = webpack(webpackConfig);
  app.use(webpackHotMiddleware(compiler));
  app.use(webpackDevMiddleware(compiler, {
    publicPath: webpackConfig.output.publicPath,
    /* Comment it if you don't want to see much info */
    // noInfo: true,
    stats: {
      colors: true,
      timings: true,
      assets: false,
      version: false,
      hash: false,
      /* Do not show chunk files here */
      // chunks: false,
      chunkModules: false,
      warnings: false
    }
  }));

  // Add static asset for DEV mode
  app.use('/asset', express.static(config.settings.paths.asset));
}

app.get("*", (req, res) => {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  if (compiler) {
    const filename = path.join(compiler.outputPath, 'index.html');
    const html = compiler.outputFileSystem.readFileSync(filename, 'utf8');
    // const html = pug.render(pugString, { initialState });

    res.set('content-type', 'text/html');
    res.send(html);
    res.end();
  }
  else {
    res.sendFile(path.join(config.settings.paths.dist, 'index.html'));
  }
});

app.listen(port, host, function(error) {
  if (error) {
    console.log("🌎 🌎 *** ERROR *** 🌎 🌎");
    console.error(error);
  }
  else {
    console.info("🌎 🌎 *** Listening at http://%s:%s *** 🌎 🌎", host, port);
  }
});
