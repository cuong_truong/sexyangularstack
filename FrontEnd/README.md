Make sure you've installed Node.js globally:
> Nodejs LTS version [Download](https://nodejs.org/en/download/)

Install global webpack
#npm i -g webpack

Install global typings
#npm i -g typings

Install dependencies
#npm i

If you face to problem while installing dependencies relating to ERR! self signed certificate in certificate chain
http://blog.npmjs.org/post/78165272245/more-help-with-selfsignedcertinchain-and-npm

Start DEVELOPMENT mode
#npm run dev

Start PRODUCTION mode
#npm run prod

Run in browser http://localhost:8000 to start