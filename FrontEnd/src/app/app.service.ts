import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Menu } from 'core';

import {
  URL_HOME,
  URL_EXAMPLES,
  URL_REGISTER,
  URL_LOGIN,
  URL_DASHBOARD,
  ICON_HOME,
  ICON_EXAMPLES,
  ICON_REGISTER,
  ICON_LOGIN,
  ICON_DASHBOARD
} from './app.constant';

@Injectable()
export class AppService {
  /**
   * Get menus data
   *
   * @return {Observable} The list of menu in observable
   * @memberof AppService
   */
  getMenus = (): Observable <Menu[]> => {
    return Observable.of([
      { link: URL_HOME, icon: ICON_HOME, label: 'Home' },
      { link: URL_EXAMPLES, icon: ICON_EXAMPLES, label: 'Examples' },
      { link: URL_LOGIN, icon: ICON_LOGIN, label: 'Login' },
      { link: URL_REGISTER, icon: ICON_REGISTER, label: 'Register' },
      { link: URL_DASHBOARD, icon: ICON_DASHBOARD, label: 'Dashboard' }
    ]);
  }
}