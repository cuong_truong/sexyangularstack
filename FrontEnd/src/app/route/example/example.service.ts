import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Menu } from 'core';

@Injectable()
export class ExampleService {
  /**
   * Get menus data
   *
   * @return {Observable} The list of menu in observable
   * @memberof ExampleService
   */
  getMenus = (): Observable <Menu[]> => {
    return Observable.of([
      { link: 'hello', label: 'Hello' },
      { link: 'counter', label: 'Counter' },
      { link: 'counter-ngrx', label: 'Counter @ngrx' },
      { link: 'user', label: 'Users' }
    ]);
  }
}