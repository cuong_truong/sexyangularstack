import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MdTabsModule } from '@angular/material';

import { exampleRoutes } from './example.route';
import { ExampleComponent } from './example.component';

import { HelloComponent } from './route/hello/hello.component';

@NgModule({
  imports: [
    CommonModule,
    MdTabsModule,
    RouterModule.forChild(exampleRoutes)
  ],
  declarations: [
    ExampleComponent,
    HelloComponent
  ]
})
export class ExampleModule {}