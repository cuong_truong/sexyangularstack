import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';

@Injectable()
export class CounterAction {
  static INCREMENT: string = 'INCREMENT';
  static DECREMENT: string = 'DECREMENT';
  static RESET: string = 'RESET';

  /**
   * Increase counter
   *
   * @memberof CounterAction
   */
  increment = () : Action => {
    return {
      type: CounterAction.INCREMENT
    };
  }

  /**
   * Decrease counter
   *
   * @memberof CounterAction
   */
  decrement = () : Action => {
    return {
      type: CounterAction.DECREMENT
    };
  }

  /**
   * Reset counter
   *
   * @memberof CounterAction
   */
  reset = () : Action => {
    return {
      type: CounterAction.RESET
    };
  }
}
