import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MdButtonModule } from '@angular/material';
import { RouterModule } from '@angular/router';

import { counterNgrxRoutes } from './counter-ngrx.route';
import { CounterNgrxComponent } from './counter-ngrx.component';

@NgModule({
  imports: [
    // If you need pipes, directives, components from a module,
    // the module should be imported into your feature module
    CommonModule,
    MdButtonModule,
    RouterModule.forChild(counterNgrxRoutes)
  ],
  declarations: [
    CounterNgrxComponent
  ]
})
export class CounterNgrxModule {}