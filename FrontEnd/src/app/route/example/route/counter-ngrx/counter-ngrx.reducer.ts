import { ActionReducer, Action } from '@ngrx/store';

import { AppState, createReducer } from 'core';

import { CounterAction } from './counter-ngrx.action';

/**
 * CouterState
 *
 * @export
 * @interface CounterState
 */
export interface CounterState {
  number: number;
};

/**
 * initialState: CounterState
 */
const initialState: CounterState = {
  number: 0
};

/**
 * App state with counter
 *
 * @export
 * @interface AppStateWithCounter
 */
export interface AppStateWithCounter extends AppState {
  counter: CounterState
}

/**
 * Function reducer that handle update counter state
 *
 * @param  {CounterState} state  The current couter state
 * @param  {Action} action        The action that indicates what should be changed
 * @return {CounterState}        The new counter state
 */
export const counterReducer = (state: CounterState = initialState, action: Action): CounterState => {
  switch (action.type) {
    case CounterAction.INCREMENT:
      return { number: state.number + 1 };

    case CounterAction.DECREMENT:
      return { number: state.number - 1 };

    case CounterAction.RESET:
      return { number: 0 };

    default:
      return state;
  }
};

/**
 * appReducerWithCounter
 *
 * @return {ActionReducer<{}>} The appReducerWithCounter
 */
export const appReducerWithCounter: ActionReducer<{}> = createReducer({
  counter: counterReducer
});