import { Routes } from '@angular/router';

import { CounterNgrxComponent } from './counter-ngrx.component';

/**
 * Export counter ngrx routes
 * @return {Array} Counter ngrx routes
 */
export const counterNgrxRoutes: Routes = [
  { path: '', component: CounterNgrxComponent }
];