import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { CounterAction } from './counter-ngrx.action';
import { AppStateWithCounter, appReducerWithCounter } from './counter-ngrx.reducer';

@Component({
  templateUrl: './counter-ngrx.component.html',
  providers: [CounterAction]
})
export class CounterNgrxComponent {
  counter: Observable<number>;

  /**
   * Creates an instance of CounterNgrxComponent.
   * @param {Store<IAppStateWithCounter>} store The app store with counter injected
   * @param {CounterAction} actions             The counter actions that describe state changes
   *
   * @memberof CounterNgrxComponent
   */
  constructor(private store: Store<AppStateWithCounter>, private actions: CounterAction) {
    this.store.replaceReducer(appReducerWithCounter);
    this.counter = store.select('counter', 'number');
  }

  /**
   * Handle on click button Increase
   *
   * @memberof CounterNgrxComponent
   */
  onClickIncrease(): void {
    this.store.dispatch(this.actions.increment());
  }

  /**
   * Handle on click button Decrease
   *
   * @memberof CounterNgrxComponent
   */
  onClickDecrease(): void {
    this.store.dispatch(this.actions.decrement());
  }

  /**
   * Handle on click button Reset
   *
   * @memberof CounterNgrxComponent
   */
  onClickReset(): void {
    this.store.dispatch(this.actions.reset());
  }
}