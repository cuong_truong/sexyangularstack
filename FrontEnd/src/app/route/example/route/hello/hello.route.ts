import { Routes } from '@angular/router';

import { HelloComponent } from './hello.component';

/**
 * Export Hello routes
 * @return {Array} Hello routes
 */
export const helloRoutes: Routes = [
  { path: '', component: HelloComponent }
];