import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloComponent } from './hello.component';
import { helloRoutes } from './hello.route';

@NgModule({
  imports: [
    RouterModule.forChild(helloRoutes)
  ],
  declarations: [
    HelloComponent
  ]
})
export class HelloModule {}