import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { User } from './user.model';

@Injectable()
export class UserService {
  constructor(private http: Http) { }

  /**
   * Get Users from API
   *
   * @returns {Observable<User[]>} User list
   *
   * @memberof UserService
   */
  getUsers(): Observable<User[]> {
    return this.http.get('http://localhost:3000/api/users').map((res) => res.json());
  }
}