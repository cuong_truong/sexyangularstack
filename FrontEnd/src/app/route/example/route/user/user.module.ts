import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MdButtonModule, MdCardModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';

import { userRoutes } from './user.route';
import { UserComponent } from './user.component';
import { UserAction } from './user.action';
import { UserService } from './user.service';
import { UserEffect } from './user.effect';

@NgModule({
  imports: [
    // If you need pipes, directives, components from a module,
    // the module should be imported into your feature module
    CommonModule,
    MdButtonModule,
    MdCardModule,
    RouterModule.forChild(userRoutes),
    EffectsModule.run(UserEffect)
  ],
  providers: [UserAction, UserService],
  declarations: [
    UserComponent
  ]
})
export class UserModule {}