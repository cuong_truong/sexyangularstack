import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { UserService } from './user.service';
import { UserAction } from './user.action';
import { User } from './user.model';

@Injectable()
export class UserEffect {
  @Effect()
  getUsers$: Observable<Action> = this.actions$
    .ofType(UserAction.GET_USERS)
    .switchMap(() => this.userService.getUsers())
    .map((users: any) => this.userAction.getUsersSuccess(users.data as User[]))
    .catch(() => of(this.userAction.getUsersFail()));

  @Effect({ dispatch: false })
  getUsersFail$: Observable<Action> = this.actions$
    .ofType(UserAction.GET_USERS_FAIL)
    .do((action: Action) => console.error(action.payload));

  constructor(
    private actions$: Actions,
    private userAction: UserAction,
    private userService: UserService
  ) {}
}
