import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Action } from '@ngrx/store';

import { User } from './user.model';

@Injectable()
export class UserAction {
  static GET_USERS: string = 'GET_USERS';
  static GET_USERS_SUCCESS: string = 'GET_USERS_SUCCESS';
  static GET_USERS_FAIL: string = 'GET_USERS_FAIL';

  /**
   * Fetch users from API
   *
   * @memberof UserAction
   */
  getUsers = () : Action => {
    return {
      type: UserAction.GET_USERS
    };
  }

  /**
   * Fetch users success
   *
   * @memberof UserAction
   */
  getUsersSuccess = (users: User[]): Action => {
    return {
      type: UserAction.GET_USERS_SUCCESS,
      payload: users
    };
  }

  /**
   * Fetch users fail
   *
   * @memberof UserAction
   */
  getUsersFail = () : Action => {
    return {
      type: UserAction.GET_USERS_FAIL
    };
  }
}
