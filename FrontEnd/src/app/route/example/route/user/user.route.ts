import { Routes } from '@angular/router';

import { UserComponent } from './user.component';

/**
 * Export User routes
 * @return {Array} User routes
 */
export const userRoutes: Routes = [
  { path: '', component: UserComponent }
];