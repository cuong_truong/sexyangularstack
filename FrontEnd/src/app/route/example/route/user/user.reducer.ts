import { ActionReducer, Action } from '@ngrx/store';

import { AppState, createReducer } from 'core';

import { User } from './user.model';
import { UserAction } from './user.action';

/**
 * UserState
 *
 * @export
 * @interface UserState
 */
export interface UserState {
  list: [User];
  loaded: boolean;
  loading: boolean;
};

/**
 * initialState: UserState
 */
const initialState: UserState = {
  list: undefined,
  loaded: false,
  loading: false
};

/**
 * App state with counter
 *
 * @export
 * @interface AppStateWithUser
 */
export interface AppStateWithUser extends AppState {
  user: UserState
}

/**
 * Function reducer that handle update counter state
 *
 * @param  {UserState} state  The current couter state
 * @param  {Action} action    The action that indicates what should be changed
 * @return {UserState}        The new counter state
 */
export const userReducer = (state: UserState = initialState, action: Action): UserState => {
  switch (action.type) {
    case UserAction.GET_USERS:
      return Object.assign({}, state, { loading: true, loaded: false });
    case UserAction.GET_USERS_SUCCESS:
      return Object.assign({}, state, { loading: false, loaded: true, list: action.payload });
    case UserAction.GET_USERS_FAIL:
    default:
      return state;
  }
};

/**
 * appReducerWithUser
 *
 * @return {ActionReducer<{}>} The appReducerWithUser
 */
export const appReducerWithUser: ActionReducer<{}> = createReducer({
  user: userReducer
});