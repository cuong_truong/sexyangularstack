import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { UserAction } from './user.action';
import { AppStateWithUser, appReducerWithUser } from './user.reducer';

@Component({
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  providers: [UserAction]
})
export class UserComponent {
  users: Observable<number>;
  loading: Observable<boolean>;

  /**
   * Creates an instance of UserComponent.
   * @param {Store<IAppStateWithUser>} store The app store with counter injected
   * @param {UserAction} actions             The counter actions that describe state changes
   *
   * @memberof UserComponent
   */
  constructor(
    private store: Store<AppStateWithUser>,
    private actions: UserAction
  ) {
    this.store.replaceReducer(appReducerWithUser);
    this.users = store.select('user', 'list');
    this.loading = store.select('user', 'loading');
  }

  /**
   * Handle on click button Get users
   *
   * @memberof UserComponent
   */
  onClickGetUsers(): void {
    this.store.dispatch(this.actions.getUsers());
  }
}