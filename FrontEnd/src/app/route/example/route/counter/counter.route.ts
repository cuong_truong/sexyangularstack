import { Routes } from '@angular/router';

import { CounterComponent } from './counter.component';

/**
 * Export counter routes
 * @return {Array} Counter routes
 */
export const counterRoutes: Routes = [
  { path: '', component: CounterComponent }
];