import { Component } from '@angular/core';

@Component({
  templateUrl: './counter.component.html'
})
export class CounterComponent {
  counter: number = 0;

  /**
   * Handle on click button Increase
   *
   * @memberof CounterComponent
   */
  onClickIncrease(): void {
    this.counter += 1;
  }

  /**
   * Handle on click button Decrease
   *
   * @memberof CounterComponent
   */
  onClickDecrease(): void {
    this.counter -= 1;
  }
}