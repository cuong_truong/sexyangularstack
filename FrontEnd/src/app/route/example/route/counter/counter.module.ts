import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MdButtonModule } from '@angular/material';

import { CounterComponent } from './counter.component';
import { counterRoutes } from './counter.route';

@NgModule({
  imports: [
    RouterModule.forChild(counterRoutes),
    MdButtonModule
  ],
  declarations: [
    CounterComponent
  ]
})
export class CounterModule {}