import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { Menu } from 'core';

import { ExampleService } from './example.service';

@Component({
  templateUrl: './example.component.html',
  providers: [ExampleService]
})
export class ExampleComponent implements OnInit {
  selectedIndex: number = 0;
  menus: Observable<Menu[]>;

  /**
   * Creates an instance of ExampleComponent.
   * @param {ExampleService} exampleService   The Example service
   * @param {Router} router                   The router info
   *
   * @memberof ExampleComponent
   */
  constructor(private exampleService: ExampleService, private router: Router) {
    this.menus = this.exampleService.getMenus();
  }

  /**
   * Override ngOnInit
   *
   * @memberof ExampleComponent
   */
  ngOnInit(): void {
    const currentUrl: string = this.router.url;

    /* Calcualte selectedIndex */
    this.menus.subscribe((data: Menu[]) => {
      data.forEach((menu: Menu, index: number) => {
        if (currentUrl.endsWith(menu.link)) {
          this.selectedIndex = index;

          return;
        }
      });
    });
  }
}