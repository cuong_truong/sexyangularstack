import { Routes } from '@angular/router';

import { ExampleComponent } from './example.component';

import { HelloComponent } from './route/hello/hello.component';

/**
 * Export example routes
 * @return {Array} Example routes
 */
export const exampleRoutes: Routes = [
  {
    path: '',
    component: ExampleComponent,
    children: [
      { path: '', redirectTo: 'hello', pathMatch: 'full' },
      { path: 'hello', component: HelloComponent },
      {
        path: 'counter',
        loadChildren: './route/counter/counter.module#CounterModule'
      },
      {
        path: 'counter-ngrx',
        loadChildren: './route/counter-ngrx/counter-ngrx.module#CounterNgrxModule'
      },
      {
        path: 'user',
        loadChildren: './route/user/user.module#UserModule'
      }
    ]
  }
];