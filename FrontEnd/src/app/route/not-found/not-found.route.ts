import { Routes } from '@angular/router';

import { NotFoundComponent } from './not-found.component';

/**
* Export Not Found routes
* @return {Array} Not Found routes
*/
export const notFoundRoutes: Routes = [
  { path: '', component: NotFoundComponent }
];