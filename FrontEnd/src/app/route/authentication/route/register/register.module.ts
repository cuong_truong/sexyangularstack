import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RegisterComponent } from './register.component';
import { registerRoutes } from './register.route';

@NgModule({
  imports: [
    RouterModule.forChild(registerRoutes)
  ],
  declarations: [
    RegisterComponent
  ]
})
export class RegisterModule {}