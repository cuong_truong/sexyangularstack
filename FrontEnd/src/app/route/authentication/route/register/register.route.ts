import { Routes } from '@angular/router';

import { RegisterComponent } from './register.component';

/**
* Export register routes
* @return {Array} Register routes
*/
export const registerRoutes: Routes = [
  { path: '', component: RegisterComponent }
];