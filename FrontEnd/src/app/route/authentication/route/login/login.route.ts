import { Routes } from '@angular/router';

import { LoginComponent } from './login.component';

/**
* Export login routes
* @return {Array} Login routes
*/
export const loginRoutes: Routes = [
  { path: '', component: LoginComponent }
];