// import {RouterModule, Routes} from '@angular/router';

// import {HomeModule} from './route/home/home.module';
// import {HomeComponent} from './route/home/home.component';
// // import {LoginModule} from './route/authentication/route/login/login.module';
// // import {LoginComponent} from './route/authentication/route/login/login.component';
// import {RegisterModule} from './route/authentication/route/register/register.module';
// import {RegisterComponent} from './route/authentication/route/register/register.component';
// import {NotFoundModule} from './route/not-found/not-found.module';
// import {NotFoundComponent} from './route/not-found/not-found.component';

// export const appRoutes: Routes = [
//   { path: 'home', component: HomeComponent },
//   {
//     path: 'login',
//     loadChildren: () => new Promise((resolve) => {
//       (require as any).ensure([], (require: any) => {
//         resolve(require('./route/authentication/route/login/login.module')['LoginModule']);
//       });
//     })
//   },
//   { path: 'register', component: RegisterComponent },
//   { path: '', redirectTo: '/home', pathMatch: 'full' },
//   { path: '**', component: NotFoundComponent }
// ];