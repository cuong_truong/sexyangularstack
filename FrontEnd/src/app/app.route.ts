import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './route/home/home.component';
import { NotFoundComponent } from './route/not-found/not-found.component';

export const appRoutes: Routes = [
  {
    path: 'example',
    loadChildren: './route/example/example.module#ExampleModule'
  },
  {
    path: 'login',
    loadChildren: './route/authentication/route/login/login.module#LoginModule'
  },
  {
    path: 'register',
    loadChildren: './route/authentication/route/register/register.module#RegisterModule'
  },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: '**', component: NotFoundComponent }
];