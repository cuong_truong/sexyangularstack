import { Component, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Menu } from 'core';

import { AppService } from './app.service';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [AppService],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  menus: Observable<Menu[]>;
  user: {} = {
    name: 'Cuong Truong',
    role: 'admin',
    avatar: 'asset/image/avatar.jpg'
  };

  /**
   * Creates an instance of AppComponent.
   * @param {AppService} appService The app service
   *
   * @memberof AppComponent
   */
  constructor(private appService: AppService) {
    this.menus = this.appService.getMenus();
  }
}