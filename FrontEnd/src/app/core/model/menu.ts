
/**
 * Menu object
 *
 * @export
 * @interface Menu
 */
export interface Menu {
  link: string;
  label: string;
  icon?: string;
}