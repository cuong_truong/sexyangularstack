import { RouterState } from '@ngrx/router-store';
import { Store, StoreModule, ActionReducer } from '@ngrx/store';

import { createReducer } from './reducer';

export interface AppState {
  router: RouterState;
}

// export class AppStoreModule {
//   static instance: AppStoreModule;
//   reducers: {};

//   constructor() {
//     if (!AppStoreModule.instance) {
//       AppStoreModule.instance = new AppStoreModule();
//     }

//     return AppStoreModule.instance;
//   }

//   create = (rootReducer: ActionReducer<{}>): AppStoreModule => {
//     AppStoreModule.instance = Store.create(rootReducer);
//     AppStoreModule.instance.reducers = rootReducer || {};

//     return AppStoreModule.instance;
//   }

//   injectReducer = (name: string, reducer: {}): void => {
//     if (AppStoreModule.instance && name && reducer) {
//       AppStoreModule.instance.reducers[name] = reducer;
//       AppStoreModule.instance.replaceReducer(createReducer(AppStoreModule.instance.reducers));
//     }
//   }
// }
