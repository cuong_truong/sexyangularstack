import { isDevMode } from '@angular/core';
import { compose } from '@ngrx/core/compose';
import { combineReducers, ActionReducer } from '@ngrx/store';
import { routerReducer } from '@ngrx/router-store';

/**
 * Default reducers
 */
const reducers: {} = {
  router: routerReducer
};

/**
 * Create root reducer
 * @param  {Object} reducers {reducers}
 * @return {type} {description}
 */
export const createReducer = (asyncReducers: {} = {}): ActionReducer<{}> => {
  if (isDevMode()) {
    return compose(combineReducers)({ ...reducers, ...asyncReducers });
  }

  return combineReducers({ ...reducers, ...asyncReducers });
};

/**
 * Root reducer
 */
export const rootReducer: ActionReducer<{}> = createReducer();