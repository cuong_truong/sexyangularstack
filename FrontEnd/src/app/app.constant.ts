/* Urls */
export const URL_HOME: string = 'home';
export const URL_EXAMPLES: string = 'example';
export const URL_LOGIN: string = 'login';
export const URL_REGISTER: string = 'register';
export const URL_DASHBOARD: string = 'dashboard';

/* Icons */
export const ICON_HOME: string = 'home';
export const ICON_EXAMPLES: string = 'explicit';
export const ICON_LOGIN: string = 'verified_user';
export const ICON_REGISTER: string = 'person_add';
export const ICON_DASHBOARD: string = 'dashboard';