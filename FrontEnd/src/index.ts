import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';

import { AppModule } from './app/app.module';

// /* Enable production mode */
// If (process.env.ENV === 'production') {
//   EnableProdMode();
// }

/* Bootstrap app module */
platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .then((success: AppModule) => console.log('BOOTSTRAP SUCCESS!'))
  .catch((err: {}) => console.error('BOOTSTRAP FAILED >>>', err));