import 'core-js/library';

import { Express, Router } from 'express';

import { AppExpressFactory } from './app.express';
import { AppRouterFactory } from './app.router';
import { AppServices } from './app.service';
import { Logger, LoggerFactory, RestErrorMiddleware } from './common';
import { AppConfig } from './config';

// Get LOGGER instance
const LOGGER: Logger = LoggerFactory.getLogger();

// Turn environment variables into a strongly typed configuration object
const appConfig: AppConfig = new AppConfig(process.env);

// Create the application data services
const appServices: AppServices = new AppServices();

// Create the application router (to be mounted by the express server)
const appRouter: Router = AppRouterFactory.getAppRouter(appServices);

// Get the application middleware (to be mounted after the app router)
const appPostMiddlewares = [
  RestErrorMiddleware.normalizeToRestError,
  RestErrorMiddleware.serializeRestError,
];

// Get Express instance
const app: Express = AppExpressFactory.getExpress(appConfig, appRouter, null, appPostMiddlewares);

// Run app on port
app.listen(appConfig.port, () => {
  LOGGER.info(`Express server listening on port ${appConfig.port}`);
});
