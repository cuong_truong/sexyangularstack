import { UserService } from './route';

export class AppServices {
  public userService: UserService;

  constructor(useMock = true) {
    if (useMock) {
      this.initMockDataServices();
    }
  }

  private initMockDataServices() {
    this.userService = new UserService();
  }
}
