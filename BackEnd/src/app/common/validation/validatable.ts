import { ValidatorError } from '../../common';

export interface Validatable {
  validate(): ValidatorError[];
  isValid(): boolean;
}