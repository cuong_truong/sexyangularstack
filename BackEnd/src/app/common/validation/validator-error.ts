export class ValidatorError {
  static readonly REQUIRED_VALIDATOR: string = 'required';

  constructor(
    public attribute: string,
    public value: any,
    public validator: string,
    public message: string
  ) {}
}