/**
 * Constraint builder
 *
 * @export
 * @class ConstraintBuilder
 */
export class ConstraintBuilder {
  private constructor() {}

  /**
   * Takes a set of constraints and strips the "required" constraint for each of
   * the specified attributes (or all attributes, if none are specified)
   *
   * @static
   * @param {*} constraints         Object constraints
   * @param {Array} keysToStrip     Array of keys to strip
   * @returns {*}                   New Object constraints
   *
   * @memberof ConstraintBuilder
   */
  static stripRequiredConstraints(constraints: any, keysToStrip: string[] = null): any {
    if (constraints == null) {
      return null;
    }

    const keysToBeStripped = keysToStrip || Object.keys(constraints);
    const newConstraints = keysToBeStripped.reduce((modifiedConstraints, key) => {
      const constraint = Object.assign({}, constraints[key]);

      delete constraint.required;
      modifiedConstraints[key] = constraint;

      return modifiedConstraints;
    }, {});

    return newConstraints;
  }
}