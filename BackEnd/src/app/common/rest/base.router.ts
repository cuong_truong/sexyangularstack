import express = require('express');
import { Router } from 'express';

import { BaseController } from './base.controller';

export abstract class BaseRouter {
  router: Router;

  constructor() {
    this.router = this.router || express.Router();
  }

  abstract initRoutes();

  wrapParamFn(controller: BaseController, handlerFn: Function) {
    return (req, res, next, param) => {
      return Promise.resolve(handlerFn.bind(controller)(req, res, next, param))
        .catch((err) => next(err));
    };
  }

  wrapRouteFn(controller: BaseController, handlerFn: Function) {
    return (req, res, next) => {
      return Promise.resolve(handlerFn.bind(controller)(req, res, next))
        .catch((err) => next(err));
    };
  }
}
