import { Request, Response } from 'express';

import { Validatable, Validator, ValidatorError } from '../validation';
import { BaseResponse } from './base.response';
import {
  BusinessViolationError,
  MethodNotAllowedError,
  ResourceNotFoundError,
  ValidationFailureError,
  ValidationFailureFactory,
} from './errors';

export class BaseController {
  respond(res: Response, item: any | any[], statusCode: number = 200): Response {
    const response = new BaseResponse(item);

    return res.status(statusCode).json(response);
  }

  respondNoContent(res: Response, statusCode: number = 204): Response {
    return res.status(statusCode).json();
  }

  validateModel(model: Validatable): void {
    const validatorErrors: ValidatorError[] = model.validate();

    this.throwValidatorErrors(validatorErrors);
  }

  validateData(data: any, constraints: any): void {
    const validatorErrors: ValidatorError[] = Validator.validate(data, constraints);

    this.throwValidatorErrors(validatorErrors);
  }

  validateResourceFound(item: any) {
      if (item == null) {
        throw new ResourceNotFoundError();
      }
  }

  throwMethodNotAllowedError(req, res, next) {
    throw new MethodNotAllowedError();
  }

  throwBusinessViolation(businessViolationCode: string, message?: string) {
    throw new BusinessViolationError(businessViolationCode, message);
  }

  // #region Private methods
  private throwValidatorErrors(validatorErrors: ValidatorError[]): void {
    if (validatorErrors == null) {
      return;
    }

    const failures = validatorErrors.map((error: ValidatorError) => {
      return ValidationFailureFactory.fromValidatorError(error);
    });

    throw new ValidationFailureError(failures);
  }
  // #endregion Private methods
}
