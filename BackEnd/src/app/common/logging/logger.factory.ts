import winston = require('winston');
import { LoggerInstance } from 'winston';

/**
 * Logger factory
 *
 * @export
 * @class LoggerFactory
 */
export class LoggerFactory {
  /**
   * Winston logger instance
   *
   * @private
   * @static
   * @type {LoggerInstance}
   * @memberof LoggerFactory
   */
  private static logger: LoggerInstance;

  /**
   * Creates an instance of LoggerFactory.
   *
   * @memberof LoggerFactory
   */
  private constructor() {}

  /**
   * This gives the appearance of creating new loggers via a factory pattern,
   * however, right now, there's no compelling reason to have multiple logger
   * instances throughout the application. As a result, it's really implementing a
   * Singleton pattern. This may change later, due to logging
   * requirements/functionality that i'm currently unaware of.
   * @static
   * @returns {LoggerInstance}
   *
   * @memberof LoggerFactory
   */
  static getLogger(): LoggerInstance {
    if (!LoggerFactory.logger) {
      const logLevel = process.env['LOG_LEVEL'];

      LoggerFactory.logger = new winston.Logger({
        transports: [new(winston.transports.Console)({level: logLevel, raw: true})]
      });
    }

    return LoggerFactory.logger;
  }
}

export { LoggerInstance as Logger };
