import { StringUtil } from '../util';

/**
 * Class contains app's config
 *
 * @export
 * @class AppConfig
 */
export class AppConfig {
  // #region Required environment variables
  port: number;           // Listen on port
  logLevel: string;       // Log level: 'debug' | 'verbose' | 'info' | 'warn' | 'error';
  serveStatic: boolean;   // Serve static or not
  // #endregion Required environment variables

  // #region Optional environment variables
  enableHttpRequestLogging: boolean;    // Http request logging
  // #endregion Optional environment variables

  /**
   * Creates an instance of AppConfig.
   * @param {*} environment
   *
   * @memberof AppConfig
   */
  constructor(private environment: any) {
    // required environment variables
    this.port = this.getIntegerEnvVar('PORT');
    this.logLevel = this.getStringEnvVar('LOG_LEVEL');
    this.serveStatic = this.getBooleanEnvVar('SERVE_STATIC');

    // optional environment variables
    this.enableHttpRequestLogging = this.getBooleanEnvVar('ENABLE_HTTP_REQUEST_LOGGING', false);
  }

  /**
   * Get environment info
   *
   * @returns {*} Environment info
   *
   * @memberof AppConfig
   */
  getEnvironment(): any {
    return this.environment;
  }

  /**
   * Get environment variable in Boolean
   *
   * @private
   * @param {string} variableName             Variable name
   * @param {boolean} [defaultValue=null]     Default value
   * @returns {boolean}                       Environment variable in Boolen
   *
   * @memberof AppConfig
   */
  private getBooleanEnvVar(variableName: string, defaultValue: boolean = null): boolean {
    const value = this.getEnvVar(variableName, defaultValue);
    const errorMessage = `Environment Variable ${variableName} does not contain a valid boolean`;

    return StringUtil.parseBoolean(value, errorMessage);
  }

  /**
   * Get environment variable in Integer
   *
   * @private
   * @param {string} variableName             Variable name
   * @param {boolean} [defaultValue]          Default value in case not found
   * @returns {number}                        Environment variable in Integer
   *
   * @memberof AppConfig
   */
  private getIntegerEnvVar(variableName: string, defaultValue: number = null): number {
    const value = this.getEnvVar(variableName, defaultValue);
    const errorMessage = `Environment Variable ${variableName} does not contain a valid integer`;

    return StringUtil.parseInt(value, errorMessage);
  }

  /**
   * Get environment variable in String
   *
   * @private
   * @param {string} variableName             Variable name
   * @param {boolean} [defaultValue=null]     Default value in case not found
   * @returns {string}                        Environment variable in String
   *
   * @memberof AppConfig
   */
  private getStringEnvVar(variableName: string, defaultValue: string = null): string {
    return this.getEnvVar(variableName, defaultValue);
  }

  /**
   * Get environment variable
   *
   * @private
   * @param {string} variableName                         Variable name
   * @param {(boolean | number | string)} defaultValue    Default value in case not found
   * @returns {string} Environment variable value
   *
   * @memberof AppConfig
   */
  private getEnvVar(variableName: string, defaultValue: boolean | number | string): string {
    const value = this.environment[variableName] || defaultValue;

    if (value == null) {
      throw new Error(`Environment Variable ${variableName} must be set!`);
    }

    return value;
  }
}
