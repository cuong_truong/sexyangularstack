import { BaseRouter } from '../../common';
import swaggerJSDoc = require('swagger-jsdoc');

export class ApiDocRouter extends BaseRouter {
  private swaggerJSDoc: any;

  public constructor() {
    super();

    this.initSwaggerJSDoc();
    this.initRoutes();
  }

  public initRoutes() {
    this.router.get('/spec.json', (req, res) => {
      res.setHeader('Content-Type', 'application/json');
      res.send(this.swaggerJSDoc);
    });
  }

  // #region Private methods
  private initSwaggerJSDoc() {
    this.swaggerJSDoc = swaggerJSDoc({
      // import swaggerDefinitions
      swaggerDefinition: {
        info: {
          swagger: '2.0',
          title: 'Sexy REST-ful APIs',
          description: 'This is a sample server using Node, Express and Swagger',
          version: '0.1.0',
          contact: {
            email: 'cuongtruong@kms-technology.com'
          },
        },
        host: 'localhost:3000',
        basePath: '/api',
        securityDefinitions: {
          Bearer: {
            type: 'apiKey',
            name: 'Authorization',
            in: 'header',
          }
        },
        tags: [
          {
            name: 'users',
            description: 'Everything about your Users',
            externalDocs: {
              description: 'Find out more',
              url: 'http://swagger.io'
            }
          }
        ],
        schemes: ['http']
      },
      // path to the API docs
      // apis: ['./build/app/route/**/*.router.js'],
      apis: ['./src/app/route/**/*.router.ts'],
    });
  }
  // #endregion Private methods
}
