import { users } from './user.data';
import { UserRepository } from './user.repository';

export class UserService {
  public userRepository: UserRepository;

  constructor(useMock = true) {
    if (useMock) {
      this.initMockDataServices();
    }
  }

  // #region Private methods
  private initMockDataServices() {
    this.userRepository = new UserRepository(users);
  }
  // #endregion Private methods
}
