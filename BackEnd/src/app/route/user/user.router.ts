import { BaseRouter } from '../../common';
import { UserController } from './user.controller';
import { UserService } from './user.service';

/**
 * Handle user router
 *
 * @export
 * @class UserRouter
 * @extends {BaseRouter}
 */
export class UserRouter extends BaseRouter {
  private userController: UserController;

  public constructor(userService: UserService) {
    super();

    this.userController = new UserController(userService);
    this.initRoutes();
  }

  public initRoutes() {
    this.router.param('userId',
      this.wrapParamFn(this.userController, this.userController.resolveUser),
    );

    /**
     * @swagger
     *  definitions:
     *    User:
     *      type: object
     *      required:
     *        - email
     *        - password
     *      properties:
     *        id:
     *          type: integer
     *          format: int64
     *        email:
     *          type: string
     *        password:
     *          type: string
     *          format: password
     *        firstName:
     *          type: string
     *        lastName:
     *          type: string
     *        userStatus:
     *          type: integer
     *          format: int16
     *          description: User status
     */

    /**
     * @swagger
     *  parameters:
     *    userBody:
     *      name: body
     *      in: body
     *      description: User info
     *      required: true
     *      schema:
     *        type: object
     *        required:
     *          - email
     *          - password
     *        properties:
     *          email:
     *            type: string
     *          password:
     *            type: string
     *            format: password
     */

    /**
     * @swagger
     *  /users:
     *    get:
     *      tags: ["users"]
     *      description: Returns user list
     *      produces:
     *        - application/json
     *      responses:
     *        200:
     *          description: Returns list of users
     *          schema:
     *            type: array
     *            items:
     *              $ref: '#/definitions/User'
     */
    this.router.get('/',
      this.wrapRouteFn(this.userController, this.userController.getAll),
    );

    /**
     * @swagger
     *  /users:
     *    post:
     *      tags: ["users"]
     *      description: Create new user
     *      produces:
     *        - application/json
     *      parameters:
     *        - $ref: '#/parameters/userBody'
     *      responses:
     *        200:
     *          description: Create user successfully
     *          schema:
     *            type: object
     *            $ref: '#/definitions/User'
     */
    this.router.post('/',
      this.wrapRouteFn(this.userController, this.userController.create),
    );
    this.router.all('/',
      this.wrapRouteFn(this.userController, this.userController.throwMethodNotAllowedError),
    );

    this.router.get('/:userId',
      this.wrapRouteFn(this.userController, this.userController.get),
    );
    this.router.delete('/:userId',
      this.wrapRouteFn(this.userController, this.userController.delete),
    );
    this.router.patch('/:userId',
      this.wrapRouteFn(this.userController, this.userController.update),
    );
    this.router.all('/:userId',
      this.wrapRouteFn(this.userController, this.userController.throwMethodNotAllowedError),
    );
  }
}
