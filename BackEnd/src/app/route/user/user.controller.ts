import { Logger, LoggerFactory, BaseController } from '../../common';

import { User } from './user.model';
import { UserService } from './user.service';

export class UserController extends BaseController {
  private static readonly LOGGER: Logger = LoggerFactory.getLogger();

  constructor(private userService: UserService) {
    super();
  }

  getAll(req, res, next): Promise<any> {
    UserController.LOGGER.debug('Retrieving all users');

    return this.userService.userRepository.getAll()
      .then((users: User[]) => {
        return this.respond(res, users);
      });
  }

  get(req, res, next): any {
    return this.respond(res, req.user);
  }

  create(req, res, next): Promise<any> {
    UserController.LOGGER.debug('Creating new user');

    const user = new User(req.body);
    this.validateModel(user);

    if (user.email === 'abc@gmail.com') {
      this.throwBusinessViolation('BLACKLISTED_EMAIL', 'This email is blacklisted.');
    }

    return this.userService.userRepository.create(user)
      .then((user: User) => {
        return this.respond(res, user);
      });
  }

  update(req, res, next: any): Promise<any> {
    UserController.LOGGER.debug('Updating new user');

    const userToUpdate: User = (req.user as User);
    userToUpdate.set(req.body);
    this.validateModel(userToUpdate);

    return this.userService.userRepository.update(userToUpdate.id, userToUpdate)
      .then((updatedUser: User) => {
        return this.respond(res, updatedUser);
      });
  }

  delete(req, res, next): Promise<any> {
    UserController.LOGGER.debug('Deleting new user');

    return this.userService.userRepository.delete(req.user.id)
      .then(() => {
        return this.respondNoContent(res);
      });
  }

  resolveUser(req, res, next, userId: string): Promise<any> {
    return this.userService.userRepository
      .get(userId)
      .then((user: User) => {
        this.validateResourceFound(user);
        req.user = user;
        next();
      });
  }
}
