import { User } from './user.model';
import { IUserRepository } from './user.irepository';

export class UserRepository implements IUserRepository {
  private userMap: Map<string, User>;
  private maxUserId: number = 0;

  constructor(users: User[]) {
    this.userMap = this.populateUserMap(users);

    const userIds = Array.from(this.userMap.keys())
      .map((val) => parseInt(val, 10));

    this.maxUserId = Math.max(...userIds);
  }

  getAll(): Promise<User[]> {
    const users = Array.from(this.userMap.values());
    return Promise.resolve(users);
  }

  get(id: string): Promise<User> {
    const user = this.userMap.get(id);

    if (user == null) { return Promise.resolve(null); }

    return Promise.resolve(user);
  }

  create(user: User): Promise<User> {
    const newUserId = ++this.maxUserId;
    user.id = newUserId.toString();

    this.userMap.set(user.id, user);
    return Promise.resolve(user);
  }

  update(id: string, user: User): Promise<User> {
    this.userMap.set(id, user);
    return Promise.resolve(user);
  }

  delete(id: string): Promise<void> {
    console.log('Deleting!');
    this.userMap.delete(id);
    return Promise.resolve();
  }

  // #region Private methods
  private populateUserMap(users: User[]): Map<string, User> {
    const usersMap = new Map<string, User>();

    users.forEach((user) => usersMap.set(user.id, user));

    return usersMap;
  }
  // #endregion Private methods
}