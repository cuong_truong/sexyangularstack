import { User } from './user.model';

export const users: User[] = [
  new User({id: '1', firstName: 'Cuong', lastName: 'Truong', email: 'cuongtruong@kms-technology.com'}),
  new User({id: '2', firstName: 'Hoa', lastName: 'Chung', email: 'chungtuyethoa@gmail.com'}),
  new User({id: '3', firstName: 'Iron', lastName: 'Man', email: 'ironman@yahoo.com'}),
  new User({id: '4', firstName: 'Captain', lastName: 'America', email: 'captainamerica@yahoo.com'}),
  new User({id: '5', firstName: 'Soldier', lastName: 'Winter', email: 'soldierwinter@aol.com'})
];