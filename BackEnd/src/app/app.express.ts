import { ErrorRequestHandler, Express, RequestHandler, Router } from 'express';

import express = require('express');
import cors = require('cors');
import bodyParser = require('body-parser');
import morgan = require('morgan');

import { Logger, LoggerFactory } from './common';
import { AppConfig } from './config';

/**
 * Express factory
 *
 * @export
 * @class ExpressFactory
 */
export class AppExpressFactory {
  /**
   * LOGGER
   *
   * @private
   * @static
   * @type {Logger}
   * @memberof ExpressFactory
   */
  private static readonly LOGGER: Logger = LoggerFactory.getLogger();

  /**
   * Get exporess instance
   *
   * @static
   * @param {AppConfig} appConfig             Configuration for app
   * @param {Router} apiRouter                API router
   * @param {Array} preApiRouterMiddlewares   API router's pre middlewares
   * @param {Array} postApiRouterMiddlewares  API router's post middlewares
   * @returns {Express}                       App express instance
   *
   * @memberof ExpressFactory
   */
  public static getExpress(
    appConfig: AppConfig,
    apiRouter: Router,
    preApiRouterMiddlewares: Array<RequestHandler | ErrorRequestHandler>,
    postApiRouterMiddlewares: Array<RequestHandler | ErrorRequestHandler>,
  ): Express {
    const app: Express = express();

    // Apply body parser
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

    //Enable cors
    app.use(cors());

    // Serve static folder
    if (appConfig.serveStatic) {
      AppExpressFactory.LOGGER.info(`Serving static files from public`);
      app.use(express.static('public'));
    }

    // Enable Http request logging
    if (appConfig.enableHttpRequestLogging) {
      AppExpressFactory.LOGGER.info(`Request logging is enabled`);
      app.use(morgan('combined'));
    }

    // Apply api router's pre middlewards
    if (preApiRouterMiddlewares != null) {
      postApiRouterMiddlewares.forEach((middlewareFn) => app.use(middlewareFn));
    }

    // Serve <domain>/api
    app.use('/api', apiRouter);

    // Apply api router's post middlewards
    if (postApiRouterMiddlewares != null) {
      postApiRouterMiddlewares.forEach((middlewareFn) => app.use(middlewareFn));
    }

    return app;
  }
}
