import express = require('express');
import { Router } from 'express';

import { InvalidResourceUrlError, Logger, LoggerFactory } from './common';
import { AppServices } from './app.service';
import { UserRouter, ApiDocRouter } from './route';

export class AppRouterFactory {
  /**
   * LOGGER
   *
   * @private
   * @static
   * @type {Logger}
   * @memberof AppRouterFactory
   */
  private static readonly LOGGER: Logger = LoggerFactory.getLogger();

  /**
   * Creates an instance of AppRouterFactory.
   *
   * @memberof AppRouterFactory
   */
  private constructor() {}

  /**
   * Get app router instance
   *
   * @static
   * @param {AppService} services       Services that used in app
   * @returns {Router}                  Object app router
   *
   * @memberof AppRouterFactory
   */
  public static getAppRouter(services: AppServices): Router {
    const router: Router = express.Router();
    const userRouter: Router = new UserRouter(services.userService).router;
    const apiDocRouter: Router = new ApiDocRouter().router;

    AppRouterFactory.LOGGER.info('Mounting users route');

    router.use('/users', userRouter);
    router.use('/swagger', apiDocRouter);

    router.all('*', (req, res, next) => {
      next(new InvalidResourceUrlError());
    });

    return router;
  }
}
