/**
 * Contains utility functions for working with strings.
 *
 * @export
 * @class StringUtil
 */
export class StringUtil {
  /**
   * Check is valid boolen
   *
   * @static
   * @param {(string | boolean)} value  Value
   * @returns {boolean}                 Is valid boolean or not
   *
   * @memberof StringUtil
   */
  static isValidBoolean(value: string | boolean): boolean {
    return (value === 'true' || value === 'false' || value === true || value === false);
  }

  /**
   * Check is valid Integer
   *
   * @static
   * @param {string} value  Value
   * @returns {boolean}     Is valid Integer or not
   *
   * @memberof StringUtil
   */
  static isValidInteger(value: string): boolean {
    const parsedInt = parseInt(value, 10);
    return !isNaN(parsedInt);
  }

  /**
   * Parse string to boolean
   *
   * @static
   * @param {string} value        Value
   * @param {string} errorMessage Error message in case parse fail
   * @returns {boolean}           Parsed value
   *
   * @memberof StringUtil
   */
  static parseBoolean(value: string, errorMessage?: string): boolean {
    if (!StringUtil.isValidBoolean(value)) {
      throw new Error(errorMessage || `${value} is not a valid boolean!`);
    }

    return StringUtil.convertStringToBoolean(value);
  }

  /**
   * Parse string to integer
   *
   * @static
   * @param {string} value        Value
   * @param {string} errorMessage Error message in case parse fail
   * @returns {number}            Parsed value
   *
   * @memberof StringUtil
   */
  static parseInt(value: string, errorMessage?: string): number {
    if (!StringUtil.isValidInteger(value)) {
      throw new Error(errorMessage || `${value} is not a valid int!`);
    }

    return parseInt(value, 10);
  }

  /**
   * Convert string to boolean
   *
   * @private
   * @static
   * @param {string} value  Value
   * @returns {boolean}     Truely/Falsy value
   *
   * @memberof StringUtil
   */
  private static convertStringToBoolean(value: string): boolean {
    return value === 'true';
  }
}